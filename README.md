# Overview

A [SikuliX](http://sikulix.com/) project folder to demostrate automated testing for the [Operator App prototype](https://bitbucket.org/mankidal19/leanback-showcase/src/master/). 
Needs the [ADB Toolkit](https://bitbucket.org/mankidal19/adbtoolkit_fortv/src/master/) and [Scrcpy](https://github.com/Genymobile/scrcpy) softwares to work as intended.

## Contents
1. [Steps for First Timer](#steps-for-first-timer)
2. [Source Code Explaination](#source-code-explaination)
3. [Video Demo](#video-demo)
---

# Steps for First Timer
1. Install [Operator App prototype](https://bitbucket.org/mankidal19/leanback-showcase/src/master/) into Android TV properly.
2. If you haven't install [Scrcpy](https://github.com/Genymobile/scrcpy), **please do so**. 
3. If you haven't install [SikuliX](http://sikulix.com/), **please do so**. 
4. Download [ADB Toolkit](https://bitbucket.org/mankidal19/adbtoolkit_fortv/src/master/):

	```git clone https://mankidal19@bitbucket.org/mankidal19/adbtoolkit_fortv.git```

5. Download this repository:

	```git clone https://mankidal19@bitbucket.org/mankidal19/sikuliautotestdemo.git```

6. Rename the downloaded repository folder to **testDemo.sikuli**.
7. Move the downloaded repository downloaded to your local SikuliX folder.
8. Run the **ADBToolKit.exe** file in ADBToolKit/bin/Debug.
9. Connect to your Android TV via ADB connect. *Must know the Android TV's IP address & PC is connected to the same network as the TV. You may use the ADB Toolkit.* 
10. Launch Scrcpy via terminal. Example of command:

	```scrcpy --birate 2M --max-size 800```
	
11. Ensure Scrcpy is currently working properly. If it does not mirror TV properly *(i.e., wrong color format or super-laggy)*, please solve the issue first. **IMPORTANT!**
12. Arrange Scrcpy & ADB ToolKit windows so they didn't overlap each other. **IMPORTANT!**
13. Launch SikuliX IDE & open this project folder.
14. You may now go through the source code & execute them for testing.

---
# Source Code Explaination
## Important methods
### beforeTest():   
*To be executed before each test case starts.*
```python
	def beforeTest():
    #focus on Scrcpy
    scrcpyApp.focus()
    #focus on adb toolkit
    adbToolApp.focus()
    #click HOME
    f.write("\nclick HOME")
    click(Pattern("adbToolAppImg.png").targetOffset(34,103))
    #wait homeUI fragment to appear
    f.write("\nwait homeUI fragment to appear")
    waitFragmentAppear("KKboxSpolify.png")
    
    #kill app, to start afresh
    f.write("\nkill app, to start afresh")
    click(Pattern("adbToolAppImg.png").targetOffset(241,-64))
    #navigate to OpApp icon & click
    f.write("\nnavigate to OpApp icon & click")
    adbToolApp.focus()
    click(Pattern("adbToolAppImg.png").targetOffset(200,103))
    click(Pattern("adbToolAppImg.png").targetOffset(200,103))
    click(Pattern("adbToolAppImg.png").targetOffset(199,68))
    #wait home fragment loaded
    f.write("\nwait home fragment loaded")
    waitFragmentAppear(Pattern("IFPTPlayWed1.png").similar(0.60))
```

### waitFragmentAppear(fragment):   
*To test either the passed fragment image is displayed or not.*
```python
	def waitFragmentAppear(fragment):
    global testStatus 
    scrcpyApp.focus()
    
    try:
        found = wait(fragment,50)
    except FindFailed:
        f.write("\nFAILED!\n")
        errorfile = takeScreenCap("error")
        f.write("Screencapture file:" + errorfile + "\n")
        Do.popError("Fragment cannot be recognized!","Sikuli Error",3)
        testStatus = False
    else:
        f.write("\nSUCCESS!\n")
        Do.popup("Fragment successfully recognized!","Sikuli Info",3)
```

### takeScreenCap(filename):   
*To take a screen capture of current screen image and save it with the desired filename (appended with the datetime taken).*
```python
	def takeScreenCap(filename):
   #focus on Scrcpy window, to solve issue scrcpyApp.focus() sometimes not working
    click("scrcpy.png")
    #capture current window
    capturedImg = capture(App.focusedWindow())

    #get current datetime info
    today = datetime.datetime.now()
    day = today.strftime("%d")
    month = today.strftime("%b")
    year = today.strftime("%Y")
    hour = today.strftime("%H")
    min = today.strftime("%M")
    sec = today.strftime("%S")

    #create a datetime string
    currTime = day + month + year + "_" +hour + min + sec

    #set screencap image file name & path
    imgFile = "screencap/" + filename + "_" + currTime + ".png"

    #move the captured image to desired path
    shutil.move(capturedImg,os.path.join(getBundlePath(), imgFile))

    #return the captured image path
    return imgFile
```

### endTest(testCase):   
*To be executed at the end of each test case. display a pop-up and write on the log file.*
```python
	def endTest(testCase):
    global testStatus
    if testStatus:
        f.write("\n Test Case " + testCase + " ended with success.")
        Do.popup("Test Case " + testCase + " ended with success.","Sikuli Test Info")

    else:
        f.write("\n Test Case " + testCase + " ended with failure.")
        Do.popError("Test Case " + testCase + " ended with failure.","Sikuli Test Info")

```

---

## Test Case 0013: 
Red/Green/Yellow/ Blue buttons on remote should act as shortcut in main browser UI.

### Flow chart:
*To be updated*

### Methods used:

### testRGYBonMainBrowser():   
*Main test method for TC0013.*
```python
	def testRGYBonMainBrowser():
    #method for TC0013
    testCase = "TC0013"
    f.write("\n" + testCase + ":\t") 
    beforeTest()
    #press red
    testButton("press red", "wait live fragment to be loaded",Pattern("adbToolAppImg.png").targetOffset(-200,-27),"liveFragment.png")
    #press green
    testButton("press green", "wait news & sports fragment to be loaded",Pattern("adbToolAppImg.png").targetOffset(-69,-26),"newsFragment.png")
    #press yellow
    testButton("press yellow", "wait tv shows fragment to be loaded",Pattern("adbToolAppImg.png").targetOffset(50,-23),"tvshow_fragment.png")
    #press blue
    testButton("press blue", "wait movies fragment to be loaded",Pattern("adbToolAppImg.png").targetOffset(201,-24),"movieFragment.png")
    #end the test case
    f.write("\n--END TEST CASE--")
    endTest(testCase)
```

### testButton(msgBefore, msgAfter, pressButton, fragmentImg):   
*To test the desired button passed and test if fragmentImg passed is displayed after pressing the button or not.*
```python
	def testButton(msgBefore, msgAfter, pressButton, fragmentImg):
    f.write("\n"+msgBefore)
    adbToolApp.focus()
    click(pressButton)
    f.write("\n"+msgAfter)
    waitFragmentAppear(fragmentImg)
```

---

## Test Case 0027: 
Changing channels using CH+ button in live TV playback.

### Flow chart:
*To be updated*

### Methods used:

### testChannelUpButton():   
*Main test method for TC0027.*
```python
	def testChannelUpButton():
    #method for TC0027
    testCase = "TC0027"
    f.write("\n" + testCase + ":\t") 
    beforeTest()

    #navigate to live tv channels
    adbToolApp.focus()
    
    f.write("\nnavigate to live tv channels")
    click(Pattern("adbToolAppImg.png").targetOffset(200,100))
    click(Pattern("adbToolAppImg.png").targetOffset(201,64))
    f.write("\nwait live fragment to be loaded")
    
    waitFragmentAppear("liveFragment.png")
    #select channel & watch now
    adbToolApp.focus()
    f.write("\nselect channel & watch now")
    click(Pattern("adbToolAppImg.png").targetOffset(201,64))
    #wait the first channel details fragment to appear
    f.write("\nwait the first channel details fragment to appear")
    waitFragmentAppear("aljazeeraDetails.png")
    click(Pattern("adbToolAppImg.png").targetOffset(201,64))
    i=1
    totalChannelAvailable = 6
    while i <= totalChannelAvailable:
        #wait video load
        message = str(i) + " out of " + str(totalChannelAvailable)
        f.write("\nwait video load " + message)
        #waitVideoLoad("livestreamEnd.png")
        testVideoLoad()
        #click CH+
        f.write("\nclick CH+")
        click(Pattern("adbToolAppImg.png").targetOffset(30,27))
        i+=1
    

    #end the test case
    f.write("\n--END TEST CASE--")
    endTest(testCase)
```

### testVideoLoad():   
*To test either video is correctly loaded or not. This method will capture the current screen and compare with the screen displayed 5 seconds later.*
*If both image are the same, test failed. Else, repeat the capture & compare steps for another 2 times as verification.*
```python
	def testVideoLoad():
    #alternative way to test video load
    
    #wait to ensure video playback view is loaded
    wait(10)
    
    global testStatus
    i=1
    totalNumOfStep = 3
    while i <= totalNumOfStep:
        #take current view screenshot
        imagePath = takeScreenCap("compare")
        #create a match object
        matchObj = Pattern(imagePath).similar(0.95)
        
        #set scrcpy window as test region
        testRegion = App.focusedWindow()
        
        #wait 5 secs
        wait(5)

        if testRegion.exists(matchObj):
            f.write("\nVIDEO LOAD FAILED!\n")
            errorfile = takeScreenCap("compare_error")
            f.write("Screencapture file:" + imagePath + " VS " + errorfile + "\n")
            Do.popError("Video load error found!","Sikuli Error",3)
            testStatus = False
            break

        else:
            itr =   str(i) + " out of " + str(totalNumOfStep) + " step"
            popMsg = "No video load error found in #" + itr + "!" 
            f.write("\nVIDEO LOAD TEST #" + itr + " SUCCESS!\n")
            Do.popup(popMsg,"Sikuli Info",3) 

        i += 1
```

---
# Video Demo
## Video Demo for TC0013
[![VIDEO DEMO FOR TC0013](https://img.youtube.com/vi/btwIQjE0CyY/0.jpg)](https://www.youtube.com/watch?v=btwIQjE0CyY)

## Video Demo for TC0027
[![VIDEO DEMO FOR TC0027](https://img.youtube.com/vi/MnQzkI1cN3A/0.jpg)](https://www.youtube.com/watch?v=MnQzkI1cN3A)
