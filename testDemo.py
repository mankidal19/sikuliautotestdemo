import datetime
import shutil
import os

f = open("testDemoLog.txt", "a")
today = datetime.datetime.now()

scrcpyApp = App("BRAVIA 4K GB")
adbToolApp = App("mankidal19's ADB Toolkit")
adbToolAppImg = "adbToolAppImg.png"

testStatus = True

#scrcpyApp.focus()
#wait(10)
#adbToolApp.focus()

def beforeTest():
    #focus on Scrcpy
    scrcpyApp.focus()
    #focus on adb toolkit
    adbToolApp.focus()
    #click HOME
    f.write("\nclick HOME")
    click(Pattern("adbToolAppImg.png").targetOffset(34,103))
    #wait homeUI fragment to appear
    f.write("\nwait homeUI fragment to appear")
    waitFragmentAppear("KKboxSpolify.png")
    
    #kill app, to start afresh
    f.write("\nkill app, to start afresh")
    click(Pattern("adbToolAppImg.png").targetOffset(241,-64))
    #navigate to OpApp icon & click
    f.write("\nnavigate to OpApp icon & click")
    adbToolApp.focus()
    click(Pattern("adbToolAppImg.png").targetOffset(200,103))
    click(Pattern("adbToolAppImg.png").targetOffset(200,103))
    click(Pattern("adbToolAppImg.png").targetOffset(199,68))
    #wait home fragment loaded
    f.write("\nwait home fragment loaded")
    waitFragmentAppear(Pattern("IFPTPlayWed1.png").similar(0.60))

def takeScreenCap(filename):
   #focus on Scrcpy window, to solve issue scrcpyApp.focus() sometimes not working
    click("scrcpy.png")
    #capture current window
    capturedImg = capture(App.focusedWindow())

    #get current datetime info
    today = datetime.datetime.now()
    day = today.strftime("%d")
    month = today.strftime("%b")
    year = today.strftime("%Y")
    hour = today.strftime("%H")
    min = today.strftime("%M")
    sec = today.strftime("%S")

    #create a datetime string
    currTime = day + month + year + "_" +hour + min + sec

    #set screencap image file name & path
    imgFile = "screencap/" + filename + "_" + currTime + ".png"

    #move the captured image to desired path
    shutil.move(capturedImg,os.path.join(getBundlePath(), imgFile))

    #return the captured image path
    return imgFile

def waitFragmentAppear(fragment):
    global testStatus 
    scrcpyApp.focus()
    
    try:
        found = wait(fragment,50)
    except FindFailed:
        f.write("\nFAILED!\n")
        errorfile = takeScreenCap("error")
        f.write("Screencapture file:" + errorfile + "\n")
        Do.popError("Fragment cannot be recognized!","Sikuli Error",3)
        testStatus = False
    else:
        f.write("\nSUCCESS!\n")
        Do.popup("Fragment successfully recognized!","Sikuli Info",3)


def waitVideoLoad(errorLoadImage):
    global testStatus
    scrcpyApp.focus()
    wait(5)
    try:
        found = wait(errorLoadImage,10)
    except FindFailed:
        f.write("\nVIDEO LOAD SUCCESS!\n")
        Do.popup("No video load error found!","Sikuli Info",3)       
    else:
        f.write("\nVIDEO LOAD FAILED!\n")
        errorfile = takeScreenCap("error")
        f.write("Screencapture file:" + errorfile + "\n")
        Do.popError("Video load error found!","Sikuli Error",3)
        testStatus = False

    waitVanish(errorLoadImage)
    

def testVideoLoad():
    #alternative way to test video load
    
    #wait to ensure video playback view is loaded
    wait(10)
    
    global testStatus
    i=1
    totalNumOfStep = 3
    while i <= totalNumOfStep:
        #take current view screenshot
        imagePath = takeScreenCap("compare")
        #create a match object
        matchObj = Pattern(imagePath).similar(0.95)
        
        #set scrcpy window as test region
        testRegion = App.focusedWindow()
        
        #wait 5 secs
        wait(5)

        if testRegion.exists(matchObj):
            f.write("\nVIDEO LOAD FAILED!\n")
            errorfile = takeScreenCap("compare_error")
            f.write("Screencapture file:" + imagePath + " VS " + errorfile + "\n")
            Do.popError("Video load error found!","Sikuli Error",3)
            testStatus = False
            break

        else:
            itr =   str(i) + " out of " + str(totalNumOfStep) + " step"
            popMsg = "No video load error found in #" + itr + "!" 
            f.write("\nVIDEO LOAD TEST #" + itr + " SUCCESS!\n")
            Do.popup(popMsg,"Sikuli Info",3) 

        i += 1

        
def testButton(msgBefore, msgAfter, pressButton, fragmentImg):
    f.write("\n"+msgBefore)
    adbToolApp.focus()
    click(pressButton)
    f.write("\n"+msgAfter)
    waitFragmentAppear(fragmentImg)
    
    


def testRGYBonMainBrowser():
    #method for TC0013
    testCase = "TC0013"
    f.write("\n" + testCase + ":\t") 
    beforeTest()
    #press red
    testButton("press red", "wait live fragment to be loaded",Pattern("adbToolAppImg.png").targetOffset(-200,-27),"liveFragment.png")
    #press green
    testButton("press green", "wait news & sports fragment to be loaded",Pattern("adbToolAppImg.png").targetOffset(-69,-26),"newsFragment.png")
    #press yellow
    testButton("press yellow", "wait tv shows fragment to be loaded",Pattern("adbToolAppImg.png").targetOffset(50,-23),"tvshow_fragment.png")
    #press blue
    testButton("press blue", "wait movies fragment to be loaded",Pattern("adbToolAppImg.png").targetOffset(201,-24),"movieFragment.png")
    #end the test case
    f.write("\n--END TEST CASE--")
    endTest(testCase)


def testChannelUpButton():
    #method for TC0027
    testCase = "TC0027"
    f.write("\n" + testCase + ":\t") 
    beforeTest()

    #navigate to live tv channels
    adbToolApp.focus()
    
    f.write("\nnavigate to live tv channels")
    click(Pattern("adbToolAppImg.png").targetOffset(200,100))
    click(Pattern("adbToolAppImg.png").targetOffset(201,64))
    f.write("\nwait live fragment to be loaded")
    
    waitFragmentAppear("liveFragment.png")
    #select channel & watch now
    adbToolApp.focus()
    f.write("\nselect channel & watch now")
    click(Pattern("adbToolAppImg.png").targetOffset(201,64))
    #wait the first channel details fragment to appear
    f.write("\nwait the first channel details fragment to appear")
    waitFragmentAppear("aljazeeraDetails.png")
    click(Pattern("adbToolAppImg.png").targetOffset(201,64))
    i=1
    totalChannelAvailable = 6
    while i <= totalChannelAvailable:
        #wait video load
        message = str(i) + " out of " + str(totalChannelAvailable)
        f.write("\nwait video load " + message)
        #waitVideoLoad("livestreamEnd.png")
        testVideoLoad()
        #click CH+
        f.write("\nclick CH+")
        click(Pattern("adbToolAppImg.png").targetOffset(30,27))
        i+=1
    

    #end the test case
    f.write("\n--END TEST CASE--")
    endTest(testCase)


def endTest(testCase):
    global testStatus
    if testStatus:
        f.write("\n Test Case " + testCase + " ended with success.")
        Do.popup("Test Case " + testCase + " ended with success.","Sikuli Test Info")

    else:
        f.write("\n Test Case " + testCase + " ended with failure.")
        Do.popError("Test Case " + testCase + " ended with failure.","Sikuli Test Info")
        
        

#main method starts here    

day = today.strftime("%A")
date = today.strftime("%x")
time = today.strftime("%X")
todayStr = day + ", " + date + " at " + time
f.write("\n\n\nTest executed on "+ todayStr + "\n")  

#testRGYBonMainBrowser()
testChannelUpButton()


if not f.closed:
    f.close()